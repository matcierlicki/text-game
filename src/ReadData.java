import java.util.Scanner;

public class ReadData {

    private static final Scanner scanner = new Scanner(System.in);

    public static String readName(String message) {
        System.out.println(message);
        return scanner.nextLine();
    }

    public static String readString(String message) {
        System.out.println(message);
        return scanner.nextLine().trim().toLowerCase();
    }
}
