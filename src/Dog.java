import java.util.ArrayList;
import java.util.Random;

public class Dog {
    private String name;
    private ArrayList<Item> items;
    private int health;

    public Dog(String name) {
        this.name = name;
        this.items = new ArrayList<>();
        this.health = 100;
    }

    public String getName() {
        return name;
    }

    public void addItem(Item item) {
        if (items.contains(item)) {
            System.out.println("Już znalazałeś: " + item.toString() + ". Szukaj pozostałych przedmiotów");
        } else {
            items.add(item);
        }

    }

    public int checkHealth() {
        return health;
    }

    public void printHealth() {
        System.out.println("Twoja ilość energii to: " + Math.max(health, 0));
    }

    public void checkItems() {
        if (items.isEmpty()) {
            System.out.println("Nie masz żadnych przedmiotów");
        } else {
            System.out.println("Towje przedmioty to: ");
            for (Item item : items) {
                System.out.println(item.toString());
            }
        }
    }

    public int checkItemSize() {
        return 3 - items.size();
    }

    public void makeNormalMove() {
        int move = drawStaminaMove();
        System.out.println("Twój ruch kosztował: " + move + " punktów energii");
        health -= move;
    }

    public void makeSpecialMove() {
        int move = drawStaminaSpecialMove();
        System.out.println("Twój ruch kosztował:" + move + " punktów energii");
        health -= move;
    }

    private int drawStaminaMove() {
        return new Random().nextInt(1, 11);
    }

    private int drawStaminaSpecialMove() {
        return new Random().nextInt(1, 20);
    }
}
