public class Game {
    private Dog dog;


    public Game(String name) {
        this.dog = new Dog(name);
    }

    public void runGame() {
        System.out.println(gameMessage());
        String command;
        System.out.println(firstMenuMessage());
        while (dog.checkHealth() > 0 && dog.checkItemSize() > 0) {
            dog.printHealth();
            command = ReadData.readString("Podaj ruch");
            switch (command) {
                case "prawo", "komoda" -> {
                    dog.makeNormalMove();
                    System.out.println(commodeMenuMessage());
                }

                case "wprost", "kanapa" -> {
                    dog.makeNormalMove();
                    System.out.println(aheadAndSofaMessage());
                }

                case "lewo", "fotel" -> {
                    dog.makeNormalMove();
                    System.out.println(chairAndLeftMessage());

                }

                case "1", "3", "pod komode" -> {
                    dog.makeSpecialMove();
                    System.out.println(firstAndThirdDrawerMessage());
                }
                case "2" -> {
                    dog.makeSpecialMove();
                    dog.addItem(Item.KUPOWORKI);
                    System.out.println(secondDrawerMessage(dog.checkItemSize()));

                }

                case "pod fotel" -> {
                    dog.makeSpecialMove();
                    dog.addItem(Item.SZELKI);
                    System.out.println(underChairMessage(dog.checkItemSize()));
                }

                case "skacz na fotel" -> {
                    dog.makeSpecialMove();
                    System.out.println(jumpOnChairMessage());
                }

                case "skacz na kanape" -> {
                    dog.makeSpecialMove();
                    System.out.println(jumpOnSofaMessage());

                }

                case "pod kanape" -> {
                    dog.makeSpecialMove();
                    System.out.println(underSofaMessage());
                }

                case "sprawdz poduszke" -> {
                    dog.makeSpecialMove();
                    dog.addItem(Item.SMYCZ);
                    System.out.println(underPillowMessage(dog.checkItemSize()));
                }

                case "przedmioty" -> {
                    dog.checkItems();
                    System.out.println(afterCheckMessage());
                }


                default -> {
                    dog.makeNormalMove();
                    System.out.println(errorMessage());
                }
            }
        }
        if (dog.checkHealth() <= 0) {
            System.out.println("NIE MASZ JUŻ ENERGII ZOSTAJESZ W DOMU");
        }
    }

    private String gameMessage() {
        return """ 
                Jesteś super psem SHIH TZU o imieniu  %s , który bardzo chce wyjść na spacer.
                Niestey, żeby wyjść na spacer musisz zgromadzić 3 przedmioty, szelki dla małego
                psa, smycz i kupoworki. Dodatkowy problem jest taki, że psy rasy SHIH TZU szybko się męczą
                i zniechęcają, a Ty bardzo chcesz wyjść na spacer. Każdy krok odbiera Ci siłę do wyjścia.
                Dodatkowo - przedmioty mogą znajdować się w trudno dostępnych miejscach co generuje
                zwiększone punkty energii. Zgromadź wszystkie 3 przedmioty jak najmniej przy tym się męcząc.
                Powodzenia dzielny psie!
                """.formatted(dog.getName());
    }

    private String firstMenuMessage() {
        return """
                Grę zaczynasz na legowisku, po prawej stronie masz komodę z szufladami, na wprost kanapę, a po lewej stronie fotel, 
                gdzie chcesz podejść ?
                Wybierz:
                PRAWO - jeśli chcesz podejść do komody
                WPROST - jeśli chcesz podejść do kanapy
                LEWO - jeśli chcesz podejść do fotela
                PRZEDMIOTY - jeśli chcesz sprawdzić jakie masz już znalezione przedmioty 
                """;
    }

    private String commodeMenuMessage() {
        return """
                Znadujsz się przed komodą, komoda jest wysoka i ma kilka szuflad. Przedmioty, których szukasz mogą znajdować się
                pod komodą albo w jednej z trzech szuflad, ale nie muszą. Pamiętaj, że sprawdzanie szuflad i wchodzenie pod komodę 
                to trudne czynności dla tak małego psa jak SHIH TZU i kosztują więcej punktów energii, dlatego staraj się rozsądnie szukać.
                Wybierz:
                1 - jeśli chcesz sprawdzić pierwszą szufladę
                2 - jeśli chcesz sprawdzić drugą szufladę
                3 - jeśli chcesz sprawdzić trzecią szufladę
                POD KOMODE - jeśli chcesz sprawdzić co jest pod komodą
                PRZEDMIOTY - jeśli chcesz sprawdzić jakie masz już znalezione przedmioty 
                """;
    }

    private String firstAndThirdDrawerMessage() {
        return """
                Niestety nie znalezniono żadnego przedmiotu, który jest Ci potrzebny do spaceru.
                Wybierz:
                1 - jeśli chcesz sprawdzić pierwszą szufladę
                2 - jeśli chcesz sprawdzić drugą szufladę
                3 - jeśli chcesz sprawdzić trzecią szufladę
                POD KOMODE - jeśli chcesz sprawdzić co jest pod komodą
                PRZEDMIOTY - jeśli chcesz sprawdzić jakie masz już znalezione przedmioty 
                """;
    }

    private String jumpOnChairMessage() {
        return """
                Niestety nie znalezniono żadnego przedmiotu, który jest Ci potrzebny do spaceru.
                Wybierz:
                POD FOTEL - jeśli chcesz sprawdzić co jest pod fotelem
                PRZEDMIOTY - jeśli chcesz sprawdzić jakie masz już znalezione przedmioty 
                """;
    }

    private String underChairMessage(int leftItem) {
        return leftItem == 0 ?
                """
                        GRATULACJE!!! ZNALAZŁEŚ WSZYSTKIE PRZEDMIOTY IDZIESZ NA SPACER!!!
                        """
                :
                """
                        Gratulacje! Udało się znaleźć SZELKI i jesteśc coraz bliżej, by wyjść na spacer.
                        Zostały ci jeszcze do odnalezienia %d przedmioty. Gdzie chcesz iść dalej?
                        Wybierz:
                        KANAPA - jeśli chcesz podejść do kanapy
                        KOMODA - jeśli chcesz podejść do komody
                        PRZEDMIOTY - jeśli chcesz sprawdzić jakie masz już znalezione przedmioty
                        """.formatted(leftItem);
    }


    private String underPillowMessage(int leftItem) {
        return leftItem == 0 ?
                """
                        GRATULACJE!!! ZNALAZŁEŚ WSZYSTKIE PRZEDMIOTY IDZIESZ NA SPACER!!!
                        """
                :
                """
                        Gratulacje udało się znaleźć SMYCZ i jesteśc coraz bliżej by wyjść na spacer.
                        Zostały ci jeszcze do odnalezienia %d przedmioty. Gdzie chcesz iść dalej?
                        Wybierz:
                        KOMODA - jeśli chcesz podejść do komody
                        FOTEL - jeśli chcesz podejść do fotela
                        PRZEDMIOTY - jeśli chcesz sprawdzić jakie masz już znalezione przedmioty
                        """.formatted(leftItem);
    }

    private String secondDrawerMessage(int leftItem) {
        return leftItem == 0 ?
                """
                        GRATULACJE!!! ZNALAZŁEŚ WSZYSTKIE PRZEDMIOTY IDZIESZ NA SPACER!!!
                        """
                :
                """
                        Gratulacje udało się znaleźć KUPOWORKI i jesteśc coraz bliżej by wyjść na spacer.
                        Zostały ci jeszcze do odnalezienia %d przedmioty. Gdzie chcesz iść dalej?
                        Wybierz:
                        KANAPA - jeśli chcesz podejść na kanapy
                        FOTEL - jeśli chcesz podejść do fotela
                        PRZEDMIOTY - jeśli chcesz sprawdzić jakie masz już znalezione przedmioty
                        """.formatted(leftItem);
    }


    private String chairAndLeftMessage() {
        return """
                Stoisz przed fotelem, który nie jest tak duży jak komoda, dlatego masz dwie opcję gdzie szukać kolejnych
                przedmiotów. Jesteś mały, ale w miarę skoczny, więc możesz wskoczyć na fotel, albo pod niego wejść i poszukać 
                tam brakujących przedmiotów. Pamiętaj, są to specjalne ruchy dlatego będą kosztować więcej energii.
                Wybierz:
                SKACZ NA FOTEL - jeśli chcesz skoczyć na fotel
                POD FOTEL - jeśli chcesz zobaczyć co jest pod fotelem
                PRZEDMIOTY - jeśli chcesz sprawdzić jakie masz już znalezione przedmioty
                """;
    }

    private String aheadAndSofaMessage() {
        return """
                Stoisz przed sofą, na której często lubisz spać i odpoczywać. Dlatego wiesz, że możesz na nią wskoczyć, 
                jak również sprawdzić co jest pod nią, bo często gubisz tam zabawki. Dostępne masz dwie możliwości, tylko pamiętaj
                skakanie i wejście pod kanapę zużywją więcej energii.
                Wybierz:
                POD KANAPE - jeśli chcesz wejść pod kanapę
                SKACZ NA KANAPE - jeśli chcesz skoczyć na kanapę
                PRZEDMIOTY - jeśli chcesz sprawdzić jakie masz już znalezione przedmioty
                """;
    }

    private String underSofaMessage() {
        return """
                Niestety nie znalezniono żadnego przedmiotu, który jest Ci potrzebny do spaceru.
                Wybierz:
                SKACZ NA KANAPE - jeśli chcesz sprawdzić co jest na kanapie
                ZDROWIE - jeśli chcesz sprawdzić ile energii Ci zostało
                PRZEDMIOTY - jeśli chcesz sprawdzić jakie masz już znalezione przedmioty
                """;
    }

    private String jumpOnSofaMessage() {
        return """
                Niestety nie znalezniono żadnego przedmiotu, który jest Ci potrzebny do spaceru,
                ale możesz sprawdzić co jest pod poduszką, ponieważ jakoś dziwnie jest uniesiona.
                Wybierz:
                SPRAWDZ PODUSZKE - jeśli chcesz sprawdzić co jest pod poduszką
                PRZEDMIOTY - jeśli chcesz sprawdzić jakie masz już znalezione przedmioty
                """;
    }


    private String afterCheckMessage() {
        return """
                Wybierz:
                KANAPA - jeślic chcesz podejść do kanapy
                KOMODA - jeśli chcesz podejść do komody
                FOTEL - jeśli chcesz podejść do fotela
                """;
    }

    private String errorMessage() {
        return """
                Wybrałeś niepoprawną instrukcję, uważaj na poprawność instukcji, niepoprawne również zabierają energię
                """;

    }
}
