public class App {
    public static void main(String[] args) {
        String name = ReadData.readName("Podaj imię zawodnika");
        Game game = new Game(name);
        game.runGame();
    }
}
